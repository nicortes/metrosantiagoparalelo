# MetroSantiagoParalelo
Programa desarrollado para el ramo de Computaci�n Paralela y Distribuida. Utiliza MPI para encontrar la ruta m�s directa entre dos estaciones de metro.

## Grupo 3
Integrantes: Nicolas Cort�s - Daniel Espinoza - Rodrigo Echeverria

## Nota:
El programa fue desarrollado en Ubuntu 18.04. Se desconoce su funcionamiento en otros sistemas.

## Modo de ejecuci�n (requiere tener OpenMPI instalado):
mpirun -np 3 ./programa -f origen destino
